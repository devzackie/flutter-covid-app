import 'package:covidapp/models/covid_model.dart';
import 'package:covidapp/services/dataservice.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class CountryCovid extends StatefulWidget {
  Countries countries;

  CountryCovid({this.countries});

  @override
  _CountryCovidState createState() => _CountryCovidState();
}

class _CountryCovidState extends State<CountryCovid> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Covid Country Report"),
      ),
      backgroundColor: Colors.grey,
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 25,
          ),
          dashBoardSum(widget.countries)
        ],
      ),
    );
  }
}

Widget dashBoardSum(Countries countries) {
  return Container(
    child: Card(
      margin: EdgeInsets.only(top: 20, bottom: 10, right: 5, left: 5),
      color: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Container(
        padding: EdgeInsets.all(8),
        margin: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(countries.country, style: TextStyle(fontSize: 25),),
                SizedBox(width: 20,),
                Image.network(
                    "https://www.countryflags.io/${countries.countryCode}/flat/64.png",
                    width: 50,
                    height: 50)
              ],
            ),
            Container(
                padding: EdgeInsets.only(top: 10, bottom: 5),
                alignment: Alignment.centerRight,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                        'Time : ${DateFormat.yMMMEd().format(DateTime.parse(countries.date))}'),
                  ],
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: cardCase("ติดเชื้อสะสม", countries.totalConfirmed),
                ),
                Expanded(
                  flex: 1,
                  child: cardCase("เสียชีวิตรวม", countries.totalDeaths),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: cardCase("เสียชีวิตวันนี้", countries.newDeaths),
                ),
                Expanded(
                    flex: 1,
                    child: cardCase("ใหม่ยืนยันแล้ว", countries.newConfirmed))
              ],
            ),
            cardCase("รักษาหาย", countries.totalRecovered)
          ],
        ),
      ),
    ),
  );
}

Widget cardCase(String caseName, dynamic caseNum) {
  final formatter = new NumberFormat("#,###", 'en_US');

  return new Card(
      elevation: 3,
      color: Colors.blueGrey,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Container(
        margin: EdgeInsets.all(5),
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              formatter.format(caseNum),
              style: TextStyle(
                  fontSize: 25,
                  color: caseName == "รักษาหาย"
                      ? Colors.greenAccent
                      : Colors.lightBlueAccent),
            ),
            Divider(
              height: 1,
              color: Colors.grey,
            ),
            Text(
              caseName,
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
      ));
}
