import 'package:covidapp/models/covid_model.dart';
import 'package:covidapp/screen/countrydetail.dart';
import 'package:covidapp/services/dataservice.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

final formatter = new NumberFormat("#,###", 'en_US');

Future<void> fetchApi() async {
  await new DataService().fetchData();
}

class _DashboardScreenState extends State<DashboardScreen> {
  var duplicateItems = List<Countries>();
  final items = List<Countries>();
  TextEditingController editingController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    fetchApi();
    super.initState();
  }

  void filterSearchResults(String query) {
    List<Countries> dummySearchList = List<Countries>();
    dummySearchList.addAll(duplicateItems);
    if (query.isNotEmpty) {
      List<Countries> dummyListData = List<Countries>();
      dummySearchList.forEach((countrie) {
        if (countrie.country.toLowerCase().contains(query.toLowerCase())) {
          dummyListData.add(countrie);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(duplicateItems);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      body: LayoutBuilder(
          builder: (context, constraints) =>
              Consumer<DataService>(builder: (context, dataService, widget) {
                Covid covid = dataService != null ? dataService.covid : null;
                if (covid != null && covid.global != null) {
                  if (items.length <= 0) {
                    duplicateItems = covid.countries;
                    items.addAll(duplicateItems);
                  }

                  return SingleChildScrollView(
                      child: ConstrainedBox(
                          constraints: BoxConstraints(
                              minWidth: constraints.maxWidth,
                              minHeight: constraints.maxHeight),
                          child: IntrinsicHeight(
                              child: Column(
                            children: <Widget>[
                              Container(
                                alignment: Alignment.centerRight,
                                padding: EdgeInsets.only(
                                    top: 30, right: 20, bottom: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      "Covid Dashboard",
                                      style: TextStyle(
                                          fontSize: 25, color: Colors.white),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Image.network(
                                      "https://image.flaticon.com/icons/png/512/2760/2760147.png",
                                      width: 50,
                                      height: 50,
                                    )
                                  ],
                                ),
                              ),
                              dashBoardSum(),
                              listCountry(items),
                              Container(
                                padding: EdgeInsets.all(10),
                                child: TextField(
                                  onChanged: filterSearchResults,
                                  controller: editingController,
                                  decoration: InputDecoration(
                                      labelText: "Search Country",
                                      hintText: "Search Country",
                                      prefixIcon: Icon(Icons.search),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(25.0)))),
                                ),
                              )
                            ],
                          ))));
                } else {
                  return FutureBuilder(
                    future: dataService.fetchData(),
                    builder: (context, state) {
                      switch (state.connectionState) {
                        case ConnectionState.waiting:
                          return Center(child: CircularProgressIndicator());
                          break;
                        case ConnectionState.active:
                          return Center(child: CircularProgressIndicator());
                          break;
                        case ConnectionState.none:
                          // TODO: Handle this case.
                          break;
                        case ConnectionState.done:
                          // TODO: Handle this case.
                          break;
                      }
                    },
                  );
                }
              })),
    );
  }
}

Widget dashBoardSum() {
  return Consumer<DataService>(
    builder: (context, dataService, widget) => Container(
      child: Card(
        elevation: 3,
        margin: EdgeInsets.only(bottom: 10, right: 5, left: 5),
        color: Colors.blueGrey,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Container(
          margin: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Container(
                  alignment: Alignment.centerRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        '${DateFormat.yMMMEd().format(DateTime.parse(dataService.covid.date))}',
                        style: TextStyle(color: Colors.grey),
                      ),
                      IconButton(
                        onPressed: () {
                          dataService.fetchData();
                        },
                        icon: Icon(
                          Icons.refresh,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: cardCase("ติดเชื้อสะสม",
                        dataService.covid.global.totalConfirmed),
                  ),
                  Expanded(
                    flex: 1,
                    child: cardCase(
                        "เสียชีวิตรวม", dataService.covid.global.totalDeaths),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: cardCase(
                        "เสียชีวิตวันนี้", dataService.covid.global.newDeaths),
                  ),
                  Expanded(
                      flex: 1,
                      child: cardCase("ใหม่ยืนยันแล้ว",
                          dataService.covid.global.newConfirmed))
                ],
              ),
              cardCase("รักษาหาย", dataService.covid.global.totalRecovered)
            ],
          ),
        ),
      ),
    ),
  );
}

Widget cardCase(String caseName, dynamic caseNum) {
  return new Card(
      color: Colors.white,
      elevation: 3,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Container(
        margin: EdgeInsets.all(1),
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              formatter.format(caseNum),
              style: TextStyle(
                  fontSize: 25,
                  color: caseName == "รักษาหาย" ? Colors.green : Colors.red),
            ),
            Divider(
              height: 1,
              color: Colors.grey,
            ),
            Text(
              caseName,
              style: TextStyle(color: Colors.grey),
            )
          ],
        ),
      ));
}

Widget listCountry(items) {
  return Expanded(
      child: SizedBox(
    height: 200,
    child: SizedBox(
      height: 200,
      child: RefreshIndicator(
        onRefresh: fetchApi,
        child: ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, index) {
              return Card(
                  elevation: 2,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: InkWell(
                    child: Container(
                        padding: EdgeInsets.only(left: 20, top: 10, bottom: 10),
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('${items[index].country}',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 20)),
                                SizedBox(
                                  width: 10,
                                ),
                                Row(
                                  children: <Widget>[
                                    Image.network(
                                        "https://www.countryflags.io/${items[index].countryCode}/flat/64.png",
                                        width: 50,
                                        height: 50),
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                      color: Colors.black12,
                                      size: 40,
                                    )
                                  ],
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Divider(
                              color: Colors.black12,
                              height: .5,
                            ),
                          ],
                        )),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CountryCovid(
                                    countries: items[index],
                                  )));
                    },
                  ));
            }),
      ),
    ),
  ));
}
