import 'dart:convert';
import 'package:covidapp/models/covid_model.dart';
import 'package:covidapp/screen/dashboard.dart';
import 'package:covidapp/services/dataservice.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: DataService()),
        ],
        child: DashboardScreen(),
      ),
    );
  }
}
