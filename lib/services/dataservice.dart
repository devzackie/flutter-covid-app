import 'package:covidapp/models/covid_model.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class DataService extends ChangeNotifier {
  Covid _covid;

  Future<void> fetchData() async {
    final resp = await http.get('https://api.covid19api.com/summary');
    print('fetch api covid ****');
    try {
      final body = jsonDecode(resp.body);
      _covid = new Covid.fromJson(body);
      notifyListeners();
    } on FormatException catch (ex) {
      _covid = null;
      notifyListeners();
    }
  }

  Covid get covid => _covid;
}
